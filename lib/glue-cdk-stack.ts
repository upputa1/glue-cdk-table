import * as cdk from '@aws-cdk/core';
import * as glue from '@aws-cdk/aws-glue';

declare const parameters: any;
declare const skewedColumnValueLocationMaps: any;

export class GlueCdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // @ts-ignore
    const cfnTable = new glue.CfnTable(this, 'MyCfnTable', {
      catalogId: '676121293094',
      databaseName: 'nws-anu-db',
      tableInput: {
        description: 'description',
        name: 'nws-anu-cdk-glue-post',
        // // owner: 'owner',
        parameters: {
          // "time_ltz.expr" : "TO_TIMESTAMP_LTZ(`event_time`, 3)",
          // "time_ltz.data-type" : "TIMESTAMP(3)",
          // "kinesisanalytics.rowtime": "time_ltz",
          "flink.json.fail-on-missing-field": "false",
          "flink.json.ignore-parse-errors" : "true",
          // "kinesisanalytics.watermark.event_time.milliseconds" : 5000,
          "classification": "json"
        },
        // partitionKeys: [{
        //   name: 'event_time',s
        //
        //   // the properties below are optional
        //   // comment: 'comment',
        //   // type: 'type',
        // }],
        // retention: 123,
        storageDescriptor: {
          // bucketColumns: ['bucketColumns'],
          // columns: [{
          //   name: 'name',
          //
          //   // the properties below are optional
          //   comment: 'comment',
          //   type: 'type',
          // }],
          compressed: false,
          inputFormat: "org.apache.hadoop.mapred.TextInputFormat",
          location: 'NWS-anu-events',
          outputFormat: 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat',
          "parameters": {
            "streamARN": "arn:aws:kinesis:us-east-1:676121293094:stream/NWS-anu-events",
            "typeOfData": "kinesis"
          },
          schemaReference: {
            schemaId: {
              // registryName: 'analytics-poc',
              schemaArn: 'arn:aws:glue:us-east-1:676121293094:schema/analytics-poc/events',
              // schemaName: 'events'
            },
            // schemaVersionId: 'schemaVersionId',
            schemaVersionNumber: 2,
          },
          serdeInfo: {
            // parameters: parameters,
            serializationLibrary: 'org.openx.data.jsonserde.JsonSerDe',
            parameters :{
              "serialization.format" :1
            }
          },
          // skewedInfo: {
          //   skewedColumnNames: ['skewedColumnNames'],
          //   skewedColumnValueLocationMaps: skewedColumnValueLocationMaps,
          //   skewedColumnValues: ['skewedColumnValues'],
          // },
          // sortColumns: [{
          //   column: 'column',
          //   sortOrder: 123,
          // }],
          // storedAsSubDirectories: false,
        },
        tableType: 'VIRTUAL_VIEW',
        // targetTable: {
        //   catalogId: '676121293094',
        //   databaseName: 'nws-anu-db',
        //   name: 'NWS-anu-cdk-events',
        // },
        // viewExpandedText: 'viewExpandedText',
        // viewOriginalText: 'viewOriginalText',
      },
    });
  }
}
